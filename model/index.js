const debug = require("debug")("lab9:model");
const mongo = require("mongoose");

const mongoDomain = "mongodb://127.0.0.1";
const dbConnStr = mongoDomain + "/lab9-chat";

let db = mongo.createConnection();
(async () => {
    try {
        await db.openUri(dbConnStr);
        debug('Connected to DB: ' + dbConnStr);
    } catch (err) {
        debug("Error connecting to DB: " + err);
    }
})();
debug('Pending DB connection');

require("./chat")(db);

module.exports = model => db.model(model);

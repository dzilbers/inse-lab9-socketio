//const path = require('path');
const debug = require('debug')('lab9:index');
const router = module.exports = require('express').Router();

/* GET home page. */
router.get('/', (req, res) => {
    debug('INFO:index.js:get /');
    // res.sendFile(path.join(__dirname,'public','index.html'));
    // //res.render('index', { title: 'Express' });
    res.redirect('/chat');
});

const router = module.exports = require('express').Router();
const debug = require('debug')('lab9:users');

router.post('/login', (req, res) => {
    debug("login user=" + req.body.username);
    debug(JSON.stringify(req.body));
    req.session.user = req.body.username;
    debug(" ID=" + req.sessionID + ": " + JSON.stringify(req.session));
    res.end();
});

router.post('/logout', (req, res) => {
    debug("logout user=" + req.session.user);
    delete req.session.user;
    debug(" ID=" + req.sessionID + ": " + JSON.stringify(req.session));
    res.end();
});

const http = require('http');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const debug = require('debug')('lab9:app');
const session = require('./session');
const socketio = require('socket.io');
const chat = require('./socket/chat');

const rootRouter = require('./routes/index');
const userRouter = require('./routes/users');
const chatRouter = require('./routes/chat');

debug('Starting web/socket.io application');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
chat(app, io);

const port = normalizePort(process.env.PORT || '8080');
app.set('port', port);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// secret for cookies, including sid cookie
const secret = "lab9 secret";

// example for inline express middleware logging - adding session middleware
app.objSession = session(secret);
app.use((req, res, next) => {
    app.objSession(req, res, function () {
        debug(req.method + ": " + req.originalUrl +
            ", session: " + !!req.session + " ID=" + req.sessionID);
        next();
    });
});

// Add basic middleware to express
app.use(logger('dev')); // Log every http message (even favicon request)

app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.gif')));

// Parsing middleware: cookies, json and url-encoded body
app.cookieParser = cookieParser(secret);
app.use(app.cookieParser);
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Static content middleware
app.use(express.static(path.join(__dirname, 'public')));
app.use('/chat', express.static(path.join(__dirname, 'chat', 'dist', 'chat')));

app.use('/', rootRouter);
app.use('/users', userRouter);
app.use('/msgs', chatRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error(`Not Found: ${req.url}`);
    err.status = 404;
    //debug(err);
    next(err);
});

// error handler
app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    //debug(err);
    res.render('error');
});

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

/**
 * Promise exception handler in case it is performed without try/catch block
 */
process.on('unhandledRejection', (reason, p) => {
    debug('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
});

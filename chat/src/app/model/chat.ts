export default class Chat {
  _id: string;
  created: Date;
  content: string;
  username: string;
  room: string;

  constructor(username: string, content: string) {
    this.username = username;
    this.content = content;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { map  } from 'rxjs/operators';
import Chat from '../model/chat';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(private http: HttpClient) { }

  login(username: string) : Observable<void> {
    console.log('chat service: login - ' + username);
    return this.http.post("/users/login", { username: username })
      .pipe(map(res => {
        console.log('chat service: login response - ' + res);
        return null;
      } ));
  }

  logout() : Observable<void> {
    console.log('chat service: logout');
    return this.http.post("/users/logout", {})
      .pipe(map(res => {
        console.log('chat service: logout response - ' + res);
        return null;
      }));
  }

  getAll(room : string) : Observable<Chat[]> {
    console.log('chat service: getAll - ' + room);
    return this.http.get('/msgs?room=' + room)
        .pipe(map(res  => {
          console.log('chat service: getAll response:');
          console.log(res);
          console.log(res as Chat[]);
          return res as Chat[];
        }));
  }
}

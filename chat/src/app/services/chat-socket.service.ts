import { Injectable } from '@angular/core';
import { Observable, fromEvent, Observer } from 'rxjs';
import { map } from 'rxjs/operators';
import * as io from 'socket.io-client';
import Chat from '../model/chat';

const port = location.port === "" ? "" : ":" + location.port;
const SERVER_URL = `http://${location.hostname}${port}/`;
const CHAT = "chat";

@Injectable({
  providedIn: 'root'
})
export class ChatSocketService {
  private socket;
  public connect$: Observable<void>;
  public connectError$: Observable<any>; // error: any
  public connectTimeout$: Observable<void>;
  public error$: Observable<any>; // error: any
  public disconnect$: Observable<string>; // reason 'io server disconnect' or 'io client disconnect'
  public reconnect$: Observable<Number>; // attempt: Number
  public reconnectAttempt$: Observable<Number>; // attempt: Number
  public reconnecting$: Observable<Number>; // attempt: Number
  public reconnectError$: Observable<any>; // error: any
  public reconnectFailed$: Observable<void>;
  public ping$: Observable<void>;
  public pong$: Observable<Number>; // ms: Number
  public observer$: Observer<void>;

  public start() : Observable<void> {
    console.log(`chat socket: start`);
    return new Observable<void>( observer => {
      console.log(`chat socket: start subsribed`);
      this.socket = io(SERVER_URL + CHAT, {
        //forceNew: false         // whether to reuse an existing connection
        //// Manager options:
        //path: "/socket.io",     // name of the path that is captured on the server side
        //reconnection:true,      // whether to reconnect automatically
        //reconnectionAttempts: Infinity, // number of reconnection attempts before giving up
        //reconnectionDelay: 1000, // how long to initially wait before attempting a new reconnection (1000).
        //                        // Affected by +/- randomizationFactor, for example the default initial delay will be
        //                        // between 500 to 1500ms.
        //reconnectionDelayMax: 5000, // maximum amount of time to wait between reconnections (5000). Each attempt
        //                        // increases the reconnection delay by 2x along with a randomization as above
        //randomizationFactor: 0.5, // 0 <= randomizationFactor <= 1
        //timeout: 20000,         // connection timeout before a connect_error and connect_timeout events are emitted
        //autoConnect: true,      // by setting this false, you have to call manager.open whenever you decide
        //query: {},              // additional query parameters that are sent when connecting a namespace (then found
        //                        // in socket.handshake.query object on the server-side)
        //parser: -,              // the parser to use, defaults to an instance of the Parser that ships with socket.io
        //// Underlying Engine.IO client options:
        //upgrade: true,          // whether the client should try to upgrade the transport from long-polling WebSocket
        //forceJSONP: false,      // forces JSONP for polling transport
        //jsonp: true,            // determines whether to use JSONP when necessary for polling. If disabled (by
        //                        // settings to false) an error will be emitted (saying “No transports available”) if
        //                        // no other transports are available. If another transport is available for opening a
        //                        // connection (e.g. WebSocket) that transport will be used instead.
        //forceBase64: false,     // forces base 64 encoding for polling transport even when XHR2 responseType is
        //                        // available and WebSocket even if the used standard supports binary.
        //enablesXDR: false,      // enables XDomainRequest for IE8 to avoid loading bar flashing with click sound. default to false because XDomainRequest has a flaw of not sending cookie.
        //timestampRequests: -,   // whether to add the timestamp with each transport request. Note: polling requests are always stamped unless this option is explicitly set to false
        //timestampParam: "t",    // the timestamp parameter
        //policyPort: 843,        // port the policy server listens on
        //transports: ['polling', 'websocket'], // a list of transports to try (in order). Engine always attempts to
        //                        // connect directly with the first one, provided the feature detection test for it
        //                        // passes.
        transports: ['websocket', 'polling'],
        //transportOptions: {},   // hash of options, indexed by transport name, overriding the common options for the
        //                        // given transport. E.g. -
        //transportOptions: {
        //  polling: {
        //    extraHeaders: {
        //      'x-clientid': 'chat'
        //    }
        //  }
        //},
        //rememberUpgrade:        // false	If true and if the previous websocket connection to the server succeeded,
        //                        // the connection attempt will bypass the normal upgrade process and will initially
        //                        // try websocket. A connection attempt following a transport error will use the normal
        //                        // upgrade process. It is recommended you turn this on only when using SSL/TLS
        //                        // connections, or if you know that your network does not block websockets.
        //onlyBinaryUpgrades: false, // whether transport upgrades should be restricted to transports supporting binary
        //                        // data
        //requestTimeout: 0,      // timeout for xhr-polling requests in milliseconds (0) (only for polling transport)
        //protocols: -,           // a list of subprotocols (see MDN reference) (only for websocket transport)
        //// Only when in NodeJS server options:
        //agent: false,           // the http.Agent to use
        //pfx: -,                 // Certificate, Private key and CA certificates to use for SSL.
        //key: -,                 // Private key to use for SSL.
        //passphrase: -,          // A string of passphrase for the private key or pfx.
        //cert: -,                // Public x509 certificate to use.
        //ca: -,                  // An authority certificate or array of authority certificates to check the remote
        //                        // host against.
        //ciphers: -,             // A string describing the ciphers to use or exclude. Consult the cipher format list
        //                        // for details on the format.
        //rejectUnauthorized: false, // If true, the server certificate is verified against the list of supplied CAs. An
        //                        // ‘error’ event is emitted if verification fails. Verification happens at the
        //                        // connection level, before the HTTP request is sent.
        //perMessageDeflate: true, // parameters of the WebSocket permessage-deflate extension (see ws module api docs).
        //                        // Set to false to disable.
        //extraHeaders: {},       // Headers that will be passed for each request to the server (via xhr-polling and via
        //                        // websockets). These values then can be used during handshake or for special proxies.
        //forceNode: false,       // Uses NodeJS implementation for websockets - even if there is a native
        //                        // Browser-Websocket available, which is preferred by default over the NodeJS
        //                        // implementation. (This is useful when using hybrid platforms like nw.js or electron)
        //localAddress: -,        // the local IP address to connect to
      });
      this.observer$ = observer;

      this.connect$ = fromEvent(this.socket, "connect");
      this.connectError$ = fromEvent(this.socket, "connect_error"); // error: any
      this.connectTimeout$ = fromEvent(this.socket, "connect_timeout");
      this.error$ = fromEvent(this.socket, "error"); // error: any
      this.disconnect$ = fromEvent(this.socket, "disconnect"); // reason 'io server disconnect' or 'io client disconnect'
      this.reconnect$ = fromEvent(this.socket, "reconnect"); // attempt: Number
      this.reconnectAttempt$ = fromEvent(this.socket, "reconnect_attempt"); // attempt: Number
      this.reconnecting$ = fromEvent(this.socket, "reconnecting"); // attempt: Number
      this.reconnectError$ = fromEvent(this.socket, "reconnect_error"); // error: any
      this.reconnectFailed$ = fromEvent(this.socket, "reconnect_failed");
      this.ping$ = fromEvent(this.socket, "ping");
      this.pong$ = fromEvent(this.socket, "pong"); // ms: Number

      this.connect$.subscribe(() => {
        console.log('chat socket: connect');
        observer.next();
      });

      this.disconnect$.subscribe(reason => {
        console.log('chat socket: ' + reason);
        this.socket = undefined;
        observer.complete();
      });

      this.connectError$.subscribe(error => {
        console.log(`chat socket: connect error - ${error}`);
        observer.error(error);
        this.socket = undefined;
        observer.complete();
      });
      this.connectTimeout$.subscribe(() => {
        console.log(`chat socket: connect timeout`);
        observer.error('connect timeout');
        this.socket = undefined;
        observer.complete();
      });
      this.error$.subscribe(error => {
        console.log(`chat socket: error - ${error}`);
        observer.error(error);
      });
      this.reconnectError$.subscribe(error => {
        console.log(`chat socket: reconnect error - ${error}`);
        observer.error(error);
      });
      this.reconnectFailed$.subscribe(() => {
        console.log(`chat socket: reconnect failed`);
        observer.error('reconnect failed');
        this.socket = undefined;
        observer.complete();
      });

      this.reconnect$.subscribe(att => {
        console.log(`chat socket: reconnect ${att} attempt`);
      });
      this.reconnectAttempt$.subscribe(att => {
        console.log(`chat socket: reconnect attempt ${att}`);
      });
      this.reconnecting$.subscribe(att => {
        console.log(`chat socket: reconnecting attempt ${att}`);
      });

      this.ping$.subscribe(() => {
        //console.log(`chat socket: ping`);
      });
      this.pong$.subscribe(ms => {
        //console.log(`chat socket: pong with latency ${ms}ms`);
      });
    });
  }

  public stop() : void {
    console.log(`chat socket: start`);
    if (this.socket !== undefined)
      this.socket.disconnect();
  }

  public isActive() : boolean { return this.socket !== undefined }

  public emitLogin(username: string) : Observable<string[]> {
    console.log(`chat socket: login ${username}`);
    if (this.socket !== undefined)
      return new Observable (observer => {
        console.log(`chat socket: login subscribed ${username}`);
        this.socket.emit("login", {username: username}, data => { observer.next(data.rooms); });
      });
    else
      return new Observable (observer => {
        console.log(`chat socket: login ${username} without socket`);
        observer.next([]);
      });
  }

  public emitLogout(username: string) : Observable<void> {
    console.log(`chat socket: logout`);
    if (this.socket !== undefined)
      return new Observable (observer => {
        console.log(`chat socket: logout subsribed`);
        this.socket.emit("logout", {username: username});
        observer.next();
      });
    else
      return new Observable (observer => {
        console.log(`chat socket: logout ${username} without socket`);
        observer.next();
      });
  }

  public emitSwitchRoom(username: string, room: string) : Observable<void> {
    console.log(`chat socket: switch room - ${username}/${room}`);
    if (this.socket !== undefined)
      return new Observable (observer => {
        console.log(`chat socket: switch room subsribed - ${username}/${room}`);
        this.socket.emit('switch', {username: username, newRoom: room});
        observer.next();
      });
    else
      return new Observable (observer => {
        console.log(`chat socket: switch room ${username}/${room} without socket`);
        observer.next();
      });
  }

  public emitMessage(event: string, data: Chat) : Observable<void> {
    console.log(`chat socket: send message ${event}`);
    if (this.socket !== undefined)
      return new Observable (observer => {
        console.log(`chat socket: send message subscribed ${event}`);
        this.socket.emit(event, data);
        observer.next();
      });
    else
      return new Observable (observer => {
        console.log(`chat socket: send message ${event} without socket`);
        observer.next();
      });
  }

  public onEvent(event: string): Observable<any> {
    console.log(`chat socket: onEvent ${event} definition`);
    if (this.socket !== undefined)
      return fromEvent(this.socket, event).pipe( map(data => {
        console.log(`chat socket: onEvent occured ${event}:`);
        console.log(data);
        return data;
      }));
    else
      return new Observable (observer => {
        console.log(`chat socket: onEvent ${event} without socket`);
        observer.next();
      });
  }
}

import {Component, OnInit} from '@angular/core';
import {ChatService} from './services/chat.service';
import {ChatSocketService as ChatSocket} from './services/chat-socket.service';
import * as $ from "jquery"

import Chat from './model/chat';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  messages: Chat[];
  room: string;
  rooms: string[];
  username: string;
  inputUser: string;
  message: string;
  selected: string;

  constructor(
    private chatService: ChatService,
    private chatSocket: ChatSocket
  ) {
  }

  ngOnInit() {
    this.init();
  }

  private init() {
    console.trace("Init data!!!");
    this.messages = [];
    this.room = "";
    this.rooms = [];
    this.username = "";
    this.inputUser = "";
    this.message = "";
    this.selected = "";
  }

  private register() {
    //Listen for new messages
    this.chatSocket.onEvent('message').subscribe(data => {
        console.log("socket.io: message");
        //Push to new message to our $scope.messages
        this.messages.push(data);
      }, err => {
        this.chatSocket.stop();
        console.log(`Error: ${err}`);
        this.init();
      });

    this.chatSocket.onEvent('joined').subscribe(data =>  {
        console.log("socket.io: joined");
        //if (data.username !== this.username) {
          let message: Chat = new Chat(data.username, "joined the room");
          this.messages.push(message);
          console.log(this.messages);
        //}
      }, err => {
        this.chatSocket.stop();
        console.log(`Error: ${err}`);
        this.init();
      });

    this.chatSocket.onEvent('left').subscribe(
      data =>  {
        console.log("socket.io: left");
        //if (data.username !== this.username) {
          let message: Chat = new Chat(data.username, "left the room");
          this.messages.push(message);
          console.log(this.messages);
        //}
      }, err => {
        this.chatSocket.stop();
        console.log(`Error: ${err}`);
        this.init();
      });

    // this.chatSocket.onEvent("disconnect").subscribe(
    //   () => {
    //     console.log("server disconnected");
    //     this.chatSocket.stop();
    //     this.init();
    //   });
  }

  tologin() {
    $('#login').css('display', 'block');
  }

  login() {
    $('#login').css('display', 'none');
    this.username = "logging in...";
    // Send POST login for "authentication"
    this.chatService.login(this.inputUser).subscribe(() => {
        console.log(`POST /login response: username=${this.username} input=${this.inputUser}`);
        // Start socket
        this.chatSocket.start().subscribe(() => {
          console.log(`chat socket activated: username=${this.username} input=${this.inputUser}`);
          // register socket events
          this.register();
          // send username to socket and get room list in the acknowledge
          this.chatSocket.emitLogin(this.inputUser).subscribe(rooms => {
            console.log(`chat socket logged in: ${rooms}\nusername=${this.username} input=${this.inputUser}`);
            this.rooms = rooms;
            // if the room list is not empty - we are successful :-)
            if (rooms.length > 0) {
              this.username = this.inputUser;
              this.inputUser = "";
              this.selected = this.room = rooms[0];
              console.log(`user in room "${this.selected}: username=${this.username} input=${this.inputUser}`);
              // get the historic messages from the room
              this.chatService.getAll(this.selected).subscribe(msgs => {
                  this.messages = msgs;
                  console.log(msgs);
                }, err => {
                  console.log(`Get messages error: ${err}`);
                  this.init();
                }
              );
            } else { // no rooms - user logs out!
              this.chatService.logout().subscribe(() => {
                  console.log("POST /logout response");
                  this.chatSocket.stop();
                  this.init();
                }, err => {
                  this.chatSocket.stop();
                  console.log(`Error: ${err}`);
                  this.init();
                });
            }
          });
        },err => { // Error on socket start
          this.init();
        },() => { // Socket closed (completed)
            this.init();
          });
      }, err => { // Error on POST login
        this.init();
      }
    )
  }

  logout() {
    console.log("emit logout");
    this.chatSocket.emitLogout(this.username).subscribe(() => {
      console.log("closing chat socket");
      this.chatSocket.stop();
      this.init();
    });
  };

  change(room: string) {
    if (!this.chatSocket.isActive()) {
      alert('Please login first!');
      return;
    }
    console.log("SWITCH: " + room);
    this.selected = room;
    this.chatSocket.emitSwitchRoom(this.username, this.selected).subscribe(() => {
        this.chatService.getAll(this.selected).subscribe(msgs => {
            this.messages = msgs;
            console.log(msgs);
            this.room = this.selected;
          }, err => {
            this.selected = this.room;
            console.log(`Cant switch room - get messages error: ${err}`);
          }
        );
      }, err => {
        this.selected = this.room;
        console.log(`Cant switch room: ${err}`);
      }
    );
  }

  send() {
    if (!this.chatSocket.isActive()) {
      this.message = "";
      alert('Please login first!');
      return;
    }
    console.log(`Sending message for user=${this.username} in room=${this.room}: ${this.message}`);
    let message = new Chat(this.username, this.message);
    message.room = this.room;
    //Notify the server that there is a new message with the message as packet
    this.chatSocket.emitMessage('message', message).subscribe(() => {
        console.log(`Message sent`);
        this.messages.push(message);
      }, err => {
        console.log(`Cant send message: ${err}`);
      }
    );
    //Empty the textarea
    this.message = "";
  };
}

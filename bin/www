#!/usr/bin/env node

/**
 * Module dependencies.
 */
const http = require('http');
const debug = require('debug')('lab9:www');
debug('Starting web/socket.io application');

(async () => {

    /**
     * Create HTTP server.
     */
    const server = http.createServer();

    try {
        app = await require('../app')(server);
    } catch (e) {
        debug(`Error in application initialization: ${e}`);
        process.exit(1);
    }

    /**
     * Get port from environment and store in Express.
     */
    const port = normalizePort(process.env.PORT || '8080');
    app.set('port', port);

    server.on('request', app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);

    /**
     * Normalize a port into a number, string, or false.
     */

    function normalizePort(val) {
        const port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        const bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    process.on('unhandledRejection', (reason, p) => {
        debug('Unhandled Rejection at: Promise', p, 'reason:', reason);
        // application specific logging, throwing an error, or other logic here
    });

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        const addr = server.address();
        const bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
        debug('Listening on ' + bind);
    }
})();